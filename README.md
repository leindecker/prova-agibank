###Prova Agibank - API Request Testing

Frameworks/Ferramentas e Tecnologias utilziadas

- Java 8x
- Maven 3.3
- Cucumber 1.2.5
- Rest-assured 3.0

#### Descrição
Para relizar as seguintes validações foi utilizada a API públic  https://jsonplaceholder.typicode.com, que provêm acesso aos seguintes métodos
- GET
- POST
- PUT
- DELETE

Para validar os métodos acima foram criados os seguintes cenários

##### Casos de Sucesso (Happy Path) 
1. Envia um POST para APi para criar novo registro
      - Nesse Método foi aborada a prática da utilização de Data Table do Cucumber (Data Provider / Data Driver)
2. Envia um GET  API para listar todos os registros
3. Envia um GET para a API para listar apenas um registro específico
4. Envia um DELETE para API para deletar um registro específico
5. Envia um PUT para APi paraa atualizar um registro específico

##### Casos de Excessão (Fail Path) 
1.Envia um DELETE para API para deletar um registro não-existente
2. Envia um GET para API para listar um registro não-existente

**Como utilizar o projeto**

####Clonar o projeto para o seu ambiente local

`$git clone $project_url`

####Comandos para executar o projeto

[========]
###### Para baixar todas as depêndicas e executar todos os cenários
`$mvn test`
######  Para executar cenários específicos (Task específicas)
Cenários Existentes (TAg's criadas):
- caso_sucesso
- caso_excessao

`$mvn test -Dcucumber.options="--tags @TAG"`

######  Relatórios
Relatórios são criados no caminho:
`target/site/cucumber-reports/cucumber-html-reports/overview-tags.html"`