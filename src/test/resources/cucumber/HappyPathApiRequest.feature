@caso_sucesso
Feature: RESt API Request Test
  As a user
  I want to perfom HTTP Request

  Scenario: POST request to the API to create a new post
    Given I have API running
    When I want to perform a POST request to create a new data
    Then I need inform the following data in order to create a new post
      | id  | name       | description |
      |  01 | Guilherme  | Teste       |
      |  02 | Leindecker | Prova       |
      |  03 | Santos     | Agibank     |
      |  04 | Bruno      | Agibank     |
      |  05 | Lopes      | Agibank     |
      |  06 | Fernando   | Agibank     |
      |  07 | Mariane    | Agibank     |
      |  08 | Teste      | Agibank     |
      |  09 | Luiz       | Agibank     |
      | 010 | Carlos     | Prova       |
    Then I validate the STATUS_CODE 201

  Scenario: GET request to the API to list all data
    Given I have API running
    When I want to perform a GET request to get all data
    Then I validate the STATUS_CODE 200

  Scenario: GET request to the API to list a specific data
    Given I have API running
    When I want to perform a GET to request only the result with id 1
    Then I validate the STATUS_CODE 200

  Scenario: DELETE request to the API to delete a specific data
    Given I have API running
    When I want to perform a DELETE request to delete the result with id 1
    Then I validate the STATUS_CODE 200

  Scenario: PUT request to the API to update a specific data
    Given I have API running
    When I want to perform a PUT request to update the result with id 1
    Then I validate the STATUS_CODE 200
