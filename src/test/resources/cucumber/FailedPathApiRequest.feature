@caso_excessao
Feature: RESt API Request Test
  As a user
  I want to perfom HTTP Request
  
    Scenario: DELETE request to the API to delete a nonexistent data
    Given I have API running
    When I want to perform a DELETE request to delete a nonexistent data
    Then I validate the STATUS_CODE 404
 
    Scenario: GET request to the API to list a nonexistent data
    Given I have API running
    When I want to perform a GET request to list a nonexistent data
    Then I validate the STATUS_CODE 404