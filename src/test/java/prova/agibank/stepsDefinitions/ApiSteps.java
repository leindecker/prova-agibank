package prova.agibank.stepsDefinitions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import gherkin.deps.com.google.gson.JsonSerializationContext;
import io.restassured.RestAssured;
// IMPORTS para o restAssured
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class ApiSteps {
	private Response response;
	private RequestSpecification request;
	private long millis = System.currentTimeMillis() % 100000;

	@Before
	public void setUp() {
		RestAssured.baseURI = "https://jsonplaceholder.typicode.com";
		RestAssured.basePath = "/posts/";
	}

	@Given("^I have API running$")
	public void i_have_API_running() throws Throwable {
		request = RestAssured.given();
	}

	@When("^I want to perform a GET request to get all data$")
	public void i_want_to_perform_a_GET_request_to_get_all_data() {
		response = request.when().get();
	}

	@Then("^I validate the STATUS_CODE (\\d+)$")
	public void i_validate_the_STATUS_CODE(int statusCode) {
		assertEquals(statusCode, response.getStatusCode());
	}

	@When("^I want to perform a POST request to create a new data$")
	public void i_want_to_perform_a_POST_request_to_create_a_new_data() {
		response = request.when().get();
	}

	
	@When("^I want to perform a GET to request only the result with id (\\d+)$")
	public void i_want_to_perform_a_GET_to_request_only_the_result_with_id(String id) {
		response = request.when().get(id);
	}

	@When("^I want to perform a DELETE request to delete the result with id (\\d+)$")
	public void i_want_to_perform_a_DELETE_request_to_delete_the_result_with_id(String id) {
		response = request.when().get(id);
	}

	@When("^I want to perform a PUT request to update the result with id (\\d+)$")
	public void i_want_to_perform_a_PUT_request_to_update_the_result_with_id(String id) {
		response = request.when().get(id);
	}

	@When("^I want to perform a DELETE request to delete a nonexistent data$")
	public void i_want_to_perform_a_DELETE_request_to_delete_a_nonexistent_data() {
		response = request.when().get(Long.toString(millis));
	}

	@When("^I want to perform a GET request to list a nonexistent data$")
	public void i_want_to_perform_a_GET_request_to_list_a_nonexistent_data() {
		response = request.when().get(Long.toString(millis));
	}

	@Then("^I need inform the following data in order to create a new post$")
	public void i_need_inform_the_following_data_in_order_to_create_a_new_post(List<DataClass> dataTable) {
		Map<String, Object> jsonAsMap = new HashMap<>();
		for (DataClass jsonObj : dataTable) {
			jsonAsMap.put("id", jsonObj.getId());
			jsonAsMap.put("name", jsonObj.getName());
			jsonAsMap.put("description", jsonObj.getDescription());
			response = request.when().contentType("application/json").body(jsonAsMap).post();
			//System.out.println(response.getBody().asString());
		}
		
	}

}
