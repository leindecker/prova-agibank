package prova.agibank.runner;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(strict = false, features = "classpath:cucumber/", format = { "pretty",
"json:target/cucumber.json" }, tags = { "~@ignore" },
glue = "prova.agibank.stepsDefinitions")

public class CucumberRunTest {
}
